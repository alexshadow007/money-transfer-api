package com.github.alexshadow007.moneytransfer.service;

import com.github.alexshadow007.moneytransfer.db.AccountMapper;
import com.github.alexshadow007.moneytransfer.model.Account;
import org.apache.ibatis.session.SqlSessionManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class AccountServiceImplTest {

    private AccountService accountService;
    private SqlSessionManager sqlSessionManager;
    private AccountMapper accountMapper;

    @BeforeEach
    void beforeEach() {
        sqlSessionManager = mock(SqlSessionManager.class);
        accountMapper = mock(AccountMapper.class);
        when(sqlSessionManager.getMapper(AccountMapper.class)).thenReturn(accountMapper);
        accountService = new AccountServiceImpl(sqlSessionManager);
    }

    @AfterEach
    void afterEach() {
        Mockito.reset(sqlSessionManager, accountMapper);
    }

    @Test
    void testAccountsReturnsEmptyList() {
        Collection<Account> expected = Collections.emptyList();
        when(accountMapper.accounts()).thenReturn(expected);
        assertThat(accountService.accounts(), is(expected));
    }

    @Test
    void testAccounts() {
        Collection<Account> expected = Collections.singletonList(new Account());
        when(accountMapper.accounts()).thenReturn(expected);
        assertThat(accountService.accounts(), is(expected));
    }

    @Test
    void testAccountReturnsEmptyOptional() {
        UUID id = UUID.randomUUID();
        Optional<Account> expected = Optional.empty();
        when(accountMapper.account(id)).thenReturn(null);
        assertThat(accountService.account(id), is(expected));
    }

    @Test
    void testAccount() {
        UUID id = UUID.randomUUID();
        Account account = new Account();
        account.setId(id);

        when(accountMapper.account(id)).thenReturn(account);
        assertThat(accountService.account(id), is(Optional.of(account)));
    }

    @Test
    void testCreate() {
        BigDecimal balance = BigDecimal.valueOf(100);
        long version = 1;
        UUID[] uuid = new UUID[1];
        Currency currency = Currency.getInstance("USD");
        doAnswer(invocation -> {
            uuid[0] = invocation.getArgument(0);
            return null;
        }).when(accountMapper).create(any(UUID.class), eq(currency));
        doAnswer(invocation -> {
            if (uuid[0] == null || !uuid[0].equals(invocation.getArgument(0))) {
                return null;
            }
            Account account = new Account();
            account.setId(uuid[0]);
            account.setBalance(balance);
            account.setVersion(version);
            return account;
        }).when(accountMapper).account(any(UUID.class));

        Account result = accountService.create(currency);
        assertThat(result, notNullValue());
        assertThat(result.getId(), is(uuid[0]));
        assertThat(result.getBalance(), is(balance));
        assertThat(result.getVersion(), is(version));
    }

    @Test
    void testDelete() {
        Account account = new Account();
        account.setId(UUID.randomUUID());
        accountService.delete(account);
        verify(accountMapper).delete(account.getId());
    }
}
