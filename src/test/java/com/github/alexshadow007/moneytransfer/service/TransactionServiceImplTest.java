package com.github.alexshadow007.moneytransfer.service;

import com.github.alexshadow007.moneytransfer.db.AccountMapper;
import com.github.alexshadow007.moneytransfer.model.Account;
import com.github.alexshadow007.moneytransfer.model.Transaction;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.UUID;

import static com.github.alexshadow007.moneytransfer.model.Transaction.Status.*;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.ZERO;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

class TransactionServiceImplTest {
    private static final Currency USD = Currency.getInstance("USD");
    private static final Currency EUR = Currency.getInstance("EUR");

    private TransactionService transactionService;
    private SqlSessionManager sqlSessionManager;
    private SqlSession sqlSession;
    private AccountMapper accountMapper;

    @BeforeEach
    void beforeEach() {
        sqlSessionManager = mock(SqlSessionManager.class);
        sqlSession = mock(SqlSession.class);
        accountMapper = mock(AccountMapper.class);
        transactionService = new TransactionServiceImpl(sqlSessionManager);
        when(sqlSessionManager.openSession(false)).thenReturn(sqlSession);
        when(sqlSession.getMapper(AccountMapper.class)).thenReturn(accountMapper);
    }

    @AfterEach
    void afterEach() {
        reset(sqlSessionManager, sqlSession, accountMapper);
    }


    @Test
    void testWithdrawSourceAccountWasNotFound() {
        UUID id = UUID.randomUUID();
        Transaction result = transactionService.withdraw(id, ZERO, USD);
        assertThat(result.getStatus(), is(DECLINED));
        verify(sqlSession, never()).commit();
        verify(sqlSession).close();
    }

    @Test
    void testWithdrawWrongCurrency() {
        Account account = account(ZERO, USD);
        Transaction result = transactionService.withdraw(account.getId(), ZERO, EUR);
        assertThat(result.getStatus(), is(DECLINED));
        verify(sqlSession, never()).commit();
        verify(sqlSession).close();
    }

    @Test
    void testWithdrawNotEnoughMoney() {
        Account account = account(ZERO, USD);
        Transaction result = transactionService.withdraw(account.getId(), TEN, USD);
        assertThat(result.getStatus(), is(DECLINED));
        verify(sqlSession, never()).commit();
        verify(sqlSession).close();
    }

    @Test
    void testWithdrawUpdateFailed() {
        Account account = account(TEN, USD);
        when(accountMapper.update(account.getId(), account.getVersion(), ZERO)).thenReturn(0);
        Transaction result = transactionService.deposit(account.getId(), TEN, USD);
        assertThat(result.getStatus(), is(FAILED));
        verify(sqlSession, never()).commit();
        verify(sqlSession).close();
    }

    @Test
    void testWithdraw() {
        Account account = account(TEN, USD);
        when(accountMapper.update(account.getId(), account.getVersion(), ZERO)).thenReturn(1);
        Transaction result = transactionService.withdraw(account.getId(), TEN, USD);
        assertThat(result.getStatus(), is(COMPLETED));
        verify(sqlSession).commit();
        verify(sqlSession).close();
    }

    @Test
    void testDepositTargetAccountWasNotFound() {
        UUID id = UUID.randomUUID();
        Transaction result = transactionService.deposit(id, ZERO, USD);
        assertThat(result.getStatus(), is(DECLINED));
        verify(sqlSession, never()).commit();
        verify(sqlSession).close();
    }

    @Test
    void testDepositWrongCurrency() {
        Account account = account(ZERO, USD);
        Transaction result = transactionService.deposit(account.getId(), ZERO, EUR);
        assertThat(result.getStatus(), is(DECLINED));
        verify(sqlSession, never()).commit();
        verify(sqlSession).close();
    }

    @Test
    void testDepositUpdateFailed() {
        Account account = account(ZERO, USD);
        when(accountMapper.update(account.getId(), account.getVersion(), ZERO)).thenReturn(0);
        Transaction result = transactionService.deposit(account.getId(), ZERO, USD);
        assertThat(result.getStatus(), is(FAILED));
        verify(sqlSession, never()).commit();
        verify(sqlSession).close();
    }

    @Test
    void testDeposit() {
        Account account = account(ZERO, USD);
        when(accountMapper.update(account.getId(), account.getVersion(), TEN)).thenReturn(1);
        Transaction result = transactionService.deposit(account.getId(), TEN, USD);
        assertThat(result.getStatus(), is(COMPLETED));
        verify(sqlSession).commit();
        verify(sqlSession).close();
    }

    @Test
    void testTransferSourceAccountWasNotFound() {
        UUID id = UUID.randomUUID();
        Account target = account(ZERO, USD);
        Transaction result = transactionService.transfer(id, target.getId(), ZERO, USD);
        assertThat(result.getStatus(), is(DECLINED));
        verify(sqlSession, never()).commit();
        verify(sqlSession).close();
    }

    @Test
    void testTransferTargetAccountWasNotFound() {
        UUID id = UUID.randomUUID();
        Account source = account(ZERO, USD);
        Transaction result = transactionService.transfer(source.getId(), id, ZERO, USD);
        assertThat(result.getStatus(), is(DECLINED));
        verify(sqlSession, never()).commit();
        verify(sqlSession).close();
    }

    @Test
    void testTransferWrongCurrency() {
        Account source = account(ZERO, USD);
        Account target = account(ZERO, USD);
        Transaction result = transactionService.transfer(source.getId(), target.getId(), ZERO, EUR);
        assertThat(result.getStatus(), is(DECLINED));
        verify(sqlSession, never()).commit();
        verify(sqlSession).close();
    }

    @Test
    void testTransferAccountsShouldHaveSameCurrency() {
        Account source = account(ZERO, USD);
        Account target = account(ZERO, EUR);
        Transaction result = transactionService.transfer(source.getId(), target.getId(), ZERO, USD);
        assertThat(result.getStatus(), is(DECLINED));
        verify(sqlSession, never()).commit();
        verify(sqlSession).close();
    }

    @Test
    void testTransferNotEnoughMoney() {
        Account source = account(ZERO, USD);
        Account target = account(ZERO, USD);
        Transaction result = transactionService.transfer(source.getId(), target.getId(), TEN, USD);
        assertThat(result.getStatus(), is(DECLINED));
        verify(sqlSession, never()).commit();
        verify(sqlSession).close();
    }

    @Test
    void testTransferSourceAccountUpdateFailed() {
        Account source = account(TEN, USD);
        Account target = account(ZERO, USD);
        when(accountMapper.update(source.getId(), source.getVersion(), ZERO)).thenReturn(0);
        Transaction result = transactionService.transfer(source.getId(), target.getId(), TEN, USD);
        assertThat(result.getStatus(), is(FAILED));
        verify(sqlSession, never()).commit();
        verify(sqlSession).close();
    }

    @Test
    void testTransferTargetAccountUpdateFailed() {
        Account source = account(TEN, USD);
        Account target = account(ZERO, USD);
        when(accountMapper.update(source.getId(), source.getVersion(), ZERO)).thenReturn(1);
        when(accountMapper.update(target.getId(), target.getVersion(), TEN)).thenReturn(0);
        Transaction result = transactionService.transfer(source.getId(), target.getId(), TEN, USD);
        assertThat(result.getStatus(), is(FAILED));
        verify(sqlSession, never()).commit();
        verify(sqlSession).close();
    }

    @Test
    void testTransfer() {
        Account source = account(TEN, USD);
        Account target = account(ZERO, USD);
        when(accountMapper.update(source.getId(), source.getVersion(), ZERO)).thenReturn(1);
        when(accountMapper.update(target.getId(), target.getVersion(), TEN)).thenReturn(1);
        Transaction result = transactionService.transfer(source.getId(), target.getId(), TEN, USD);
        assertThat(result.getStatus(), is(COMPLETED));
        verify(sqlSession).commit();
        verify(sqlSession).close();
    }

    private Account account(BigDecimal balance, Currency currency) {
        UUID id = UUID.randomUUID();
        Account ret = new Account();
        ret.setId(id);
        ret.setVersion(1L);
        ret.setBalance(balance);
        ret.setCurrency(currency);

        when(accountMapper.account(id)).thenReturn(ret);

        return ret;
    }
}
