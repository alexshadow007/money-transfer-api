package com.github.alexshadow007.moneytransfer.rest;

import com.github.alexshadow007.moneytransfer.ConfigModule;
import com.github.alexshadow007.moneytransfer.db.DbModule;
import com.github.alexshadow007.moneytransfer.model.Account;
import com.github.alexshadow007.moneytransfer.model.AccountDto;
import com.github.alexshadow007.moneytransfer.service.AccountService;
import com.github.alexshadow007.moneytransfer.service.ServiceModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.zaxxer.hikari.HikariDataSource;
import io.restassured.RestAssured;
import io.restassured.mapper.TypeRef;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import spark.Service;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Currency;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.github.alexshadow007.moneytransfer.rest.Statuses.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;

class AccountControllerTest {

    private static Injector injector;
    private static AccountService accountService;

    @BeforeAll
    static void beforeAll() {
        injector = Guice.createInjector(
                new ConfigModule(),
                new DbModule(),
                new ServiceModule(),
                new RestModule()
        );

        injector.getInstance(AccountController.class).init();
        injector.getInstance(ErrorController.class).init();
        Service spark = injector.getInstance(Service.class);
        spark.awaitInitialization();

        RestAssured.port = spark.port();
        RestAssured.basePath = "/api/v1/";

        accountService = injector.getInstance(AccountService.class);
    }

    @AfterAll
    static void afterAll() throws InterruptedException {
        ((HikariDataSource)injector.getInstance(DataSource.class)).close();
        injector.getInstance(Service.class).stop();
        Thread.sleep(TimeUnit.SECONDS.toMillis(1));
    }

    @Test
    void testCreateAccountWithBadJson() {
        given().body("{a")
                .when().post("/accounts")
                .then().statusCode(BAD_REQUEST);
    }

    @Test
    void testCreateAccountWithoutCurrency() {
        given().body("{}")
                .when().post("/accounts")
                .then().statusCode(BAD_REQUEST);
    }

    @Test
    void testCreateAccountWithWrongCurrency() {
        given().body("{\"currency\":\"U123\"}")
                .when().post("/accounts")
                .then().statusCode(BAD_REQUEST);
    }

    @Test
    void testCreateAccount() {
        AccountDto result = given().body("{\"currency\":\"USD\"}")
                .when().post("accounts")
                .then().statusCode(Statuses.CREATED)
                .extract().body().as(AccountDto.class);

        assertThat(result.getCurrency(), is("USD"));
        assertThat(result.getBalance(), is(0.0));

        Optional<Account> created = accountService.account(UUID.fromString(result.getId()));
        assertThat(created.isPresent(), is(true));
        assertThat(created.get().getVersion(), is(1L));
        assertThat(created.get().getBalance(), is(new BigDecimal("0.0000")));
        assertThat(created.get().getCurrency(), is(Currency.getInstance("USD")));
    }

    @Test
    void testGetAccounts() {
        Account expected = accountService.create(Currency.getInstance("USD"));
        Collection<AccountDto> accounts = given()
                .get("accounts")
                .then().statusCode(OK)
                .extract().body().as(new TypeRef<Collection<AccountDto>>() {});
        assertThat(accounts.contains(new AccountDto(expected)), is(true));
    }

    @Test
    void testGetAccountWithWrongId() {
        given().pathParam("id", "fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae")
                .when().get("accounts/{id}")
                .then().statusCode(NOT_FOUND);
    }

    @Test
    void testGetAccount() {
        Account expected = accountService.create(Currency.getInstance("USD"));
        AccountDto result = given().pathParam("id", expected.getId().toString())
                .when().get("accounts/{id}")
                .then().statusCode(OK)
                .extract().body().as(AccountDto.class);

        assertThat(result.getId(), is(expected.getId().toString()));
        assertThat(result.getCurrency(), is(expected.getCurrency().toString()));
        assertThat(result.getBalance(), is(expected.getBalance().doubleValue()));
    }

    @Test
    void testDeleteNotExistedAccount() {
        given().pathParam("id", "fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae")
                .when().delete("accounts/{id}")
                .then().statusCode(NOT_FOUND);
    }

    @Test
    void testDeleteAccount() {
        Account account = accountService.create(Currency.getInstance("USD"));
        AccountDto result = given().pathParam("id", account.getId().toString())
                .when().delete("accounts/{id}")
                .then().statusCode(OK)
                .extract().body().as(AccountDto.class);

        assertThat(result.getId(), is(account.getId().toString()));
        assertThat(result.getCurrency(), is(account.getCurrency().toString()));
        assertThat(result.getBalance(), is(account.getBalance().doubleValue()));

        assertThat(accountService.account(account.getId()).isPresent(), is(false));
    }
}
