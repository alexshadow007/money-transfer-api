package com.github.alexshadow007.moneytransfer.rest;

import com.github.alexshadow007.moneytransfer.ConfigModule;
import com.github.alexshadow007.moneytransfer.db.DbModule;
import com.github.alexshadow007.moneytransfer.model.Account;
import com.github.alexshadow007.moneytransfer.model.TransactionDto;
import com.github.alexshadow007.moneytransfer.service.AccountService;
import com.github.alexshadow007.moneytransfer.service.ServiceModule;
import com.github.alexshadow007.moneytransfer.service.TransactionService;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.zaxxer.hikari.HikariDataSource;
import io.restassured.RestAssured;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import spark.Service;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class TransactionControllerTest {
    private static final Currency USD = Currency.getInstance("USD");

    private static Injector injector;
    private static AccountService accountService;
    private static TransactionService transactionService;

    @BeforeAll
    static void beforeAll() {
        injector = Guice.createInjector(
                new ConfigModule(),
                new DbModule(),
                new ServiceModule(),
                new RestModule()
        );

        injector.getInstance(TransactionController.class).init();
        injector.getInstance(ErrorController.class).init();
        Service spark = injector.getInstance(Service.class);
        spark.awaitInitialization();

        RestAssured.port = spark.port();
        RestAssured.basePath = "/api/v1/";

        accountService = injector.getInstance(AccountService.class);
        transactionService = injector.getInstance(TransactionService.class);
    }

    @AfterAll
    static void afterAll() throws InterruptedException {
        ((HikariDataSource)injector.getInstance(DataSource.class)).close();
        injector.getInstance(Service.class).stop();
        Thread.sleep(TimeUnit.SECONDS.toMillis(1));
    }

    @Test
    void testDepositWithWrongJson() {
        given().body("{w")
                .when().post("transactions/deposit")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testDepositWithoutTargetId() {
        given().body("{\"currency\":\"USD\", \"amount\":100.0}")
                .when().post("transactions/deposit")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testDepositWithWrongTargetId() {
        given().body("{\"currency\":\"USD\", \"amount\":100.0, \"target\":\"wrong\"}")
                .when().post("transactions/deposit")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testDepositWithoutCurrency() {
        given().body("{\"amount\":100.0, \"target\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\"}")
                .when().post("transactions/deposit")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testDepositWithWrongCurrency() {
        given().body("{\"currency\":\"U122\",\"amount\":100.0, \"target\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\"}")
                .when().post("transactions/deposit")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testDepositWithoutAmount() {
        given().body("{\"currency\":\"USD\", \"target\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\"}")
                .when().post("transactions/deposit")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testDepositAccountWasNotFound() {
        TransactionDto result = given().body("{\"currency\":\"USD\",\"amount\":100.0, \"target\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\"}")
                .when().post("transactions/deposit")
                .then().statusCode(Statuses.CREATED)
                .extract().body().as(TransactionDto.class);

        assertThat(result.getStatus(), is("DECLINED"));
        assertThat(result.getReason(), is("Account with id [fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae] was not found"));
    }

    @Test
    void testDepositRequiredSameCurrency() {
        Account account = accountService.create(USD);
        TransactionDto result = given().body("{\"currency\":\"EUR\",\"amount\":100.0, \"target\":\"" + account.getId().toString() +"\"}")
                .when().post("transactions/deposit")
                .then().statusCode(Statuses.CREATED)
                .extract().body().as(TransactionDto.class);

        assertThat(result.getStatus(), is("DECLINED"));
        assertThat(result.getReason(), is("Currency must be [USD]"));
    }

    @Test
    void testDeposit() {
        Account account = accountService.create(USD);
        TransactionDto result = given().body("{\"currency\":\"USD\",\"amount\":100.0, \"target\":\"" + account.getId().toString() +"\"}")
                .when().post("transactions/deposit")
                .then().statusCode(Statuses.CREATED)
                .extract().body().as(TransactionDto.class);

        assertThat(result.getStatus(), is("COMPLETED"));
        assertThat(accountService.account(account.getId()).get().getBalance(), is(new BigDecimal("100.0000")));
    }

    @Test
    void testWithdrawWithWrongJson() {
        given().body("{w")
                .when().post("transactions/withdraw")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testWithdrawWithoutSourceId() {
        given().body("{\"currency\":\"USD\", \"amount\":100.0}")
                .when().post("transactions/withdraw")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testWithdrawWithWrongSourceId() {
        given().body("{\"currency\":\"USD\", \"amount\":100.0, \"source\":\"wrong\"}")
                .when().post("transactions/deposit")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testWithdrawWithoutCurrency() {
        given().body("{\"amount\":100.0, \"source\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\"}")
                .when().post("transactions/withdraw")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testWithdrawWithWrongCurrency() {
        given().body("{\"currency\":\"U122\",\"amount\":100.0, \"source\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\"}")
                .when().post("transactions/withdraw")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testWithdrawWithoutAmount() {
        given().body("{\"currency\":\"USD\", \"source\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\"}")
                .when().post("transactions/withdraw")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testWithdrawAccountWasNotFound() {
        TransactionDto result = given().body("{\"currency\":\"USD\",\"amount\":100.0, \"source\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\"}")
                .when().post("transactions/withdraw")
                .then().statusCode(Statuses.CREATED)
                .extract().body().as(TransactionDto.class);

        assertThat(result.getStatus(), is("DECLINED"));
        assertThat(result.getReason(), is("Account with id [fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae] was not found"));
    }

    @Test
    void testWithdrawRequiredSameCurrency() {
        Account account = accountService.create(USD);
        TransactionDto result = given().body("{\"currency\":\"EUR\",\"amount\":100.0, \"source\":\"" + account.getId().toString() +"\"}")
                .when().post("transactions/withdraw")
                .then().statusCode(Statuses.CREATED)
                .extract().body().as(TransactionDto.class);

        assertThat(result.getStatus(), is("DECLINED"));
        assertThat(result.getReason(), is("Currency must be [USD]"));
    }

    @Test
    void testWithdrawNotEnoughMoney() {
        Account account = accountService.create(USD);
        TransactionDto result = given().body("{\"currency\":\"USD\",\"amount\":100.0, \"source\":\"" + account.getId().toString() +"\"}")
                .when().post("transactions/withdraw")
                .then().statusCode(Statuses.CREATED)
                .extract().body().as(TransactionDto.class);

        assertThat(result.getStatus(), is("DECLINED"));
        assertThat(result.getReason(), is("Balance is less than requested amount"));
    }

    @Test
    void testWithdraw() {
        Account account = accountService.create(USD);
        transactionService.deposit(account.getId(), BigDecimal.valueOf(150.0), USD);
        TransactionDto result = given().body("{\"currency\":\"USD\",\"amount\":100.0, \"source\":\"" + account.getId().toString() +"\"}")
                .when().post("transactions/withdraw")
                .then().statusCode(Statuses.CREATED)
                .extract().body().as(TransactionDto.class);

        assertThat(result.getStatus(), is("COMPLETED"));
        assertThat(accountService.account(account.getId()).get().getBalance(), is(new BigDecimal("50.0000")));
    }

    @Test
    void testTransferWithWrongJson() {
        given().body("{w")
                .when().post("transactions/transfer")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testTransferWithoutSourceId() {
        given().body("{\"currency\":\"USD\", \"amount\":100.0, \"target\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\"}")
                .when().post("transactions/transfer")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testTransferWithWrongSourceId() {
        given().body("{\"currency\":\"USD\", \"amount\":100.0, \"target\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\", \"source\":\"wrong\"}")
                .when().post("transactions/transfer")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testTransferWithoutTargetId() {
        given().body("{\"currency\":\"USD\", \"amount\":100.0, \"source\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\"}")
                .when().post("transactions/transfer")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testTransferWithWrongTargetId() {
        given().body("{\"currency\":\"USD\", \"amount\":100.0, \"source\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\", \"taget\":\"wrong\"}")
                .when().post("transactions/transfer")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testTransferWithoutCurrency() {
        given().body("{\"amount\":100.0 , \"target\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\", \"source\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\"}")
                .when().post("transactions/transfer")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testTransferWithWrongCurrency() {
        given().body("{\"currency\":\"U122\",\"amount\":100.0, \"target\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\", \"source\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\"}")
                .when().post("transactions/transfer")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testTransferWithoutAmount() {
        given().body("{\"currency\":\"USD\", \"target\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\", \"source\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\"}")
                .when().post("transactions/transfer")
                .then().statusCode(Statuses.BAD_REQUEST);
    }

    @Test
    void testTransferSourceAccountWasNotFound() {
        Account target = accountService.create(USD);
        TransactionDto result = given().body("{\"currency\":\"USD\",\"amount\":100.0, \"target\":\"" + target.getId().toString() + "\", \"source\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\"}")
                .when().post("transactions/transfer")
                .then().statusCode(Statuses.CREATED)
                .extract().body().as(TransactionDto.class);

        assertThat(result.getStatus(), is("DECLINED"));
        assertThat(result.getReason(), is("Account with id [fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae] was not found"));
    }

    @Test
    void testTransferTargetAccountWasNotFound() {
        Account source = accountService.create(USD);
        TransactionDto result = given().body("{\"currency\":\"USD\",\"amount\":100.0, \"source\":\"" + source.getId().toString() + "\", \"target\":\"fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae\"}")
                .when().post("transactions/transfer")
                .then().statusCode(Statuses.CREATED)
                .extract().body().as(TransactionDto.class);

        assertThat(result.getStatus(), is("DECLINED"));
        assertThat(result.getReason(), is("Account with id [fa42d6f9-a2dd-4fc7-93e9-6af29fa053ae] was not found"));
    }

    @Test
    void testWithdrawRequiredSameCurrencyAsSource() {
        Account source = accountService.create(USD);
        Account target = accountService.create(USD);
        TransactionDto result = given().body("{\"currency\":\"EUR\",\"amount\":100.0, \"source\":\"" + source.getId().toString() +"\", \"target\":\"" + target.getId().toString() +"\"}")
                .when().post("transactions/transfer")
                .then().statusCode(Statuses.CREATED)
                .extract().body().as(TransactionDto.class);

        assertThat(result.getStatus(), is("DECLINED"));
        assertThat(result.getReason(), is("Currency must be [USD]"));
    }

    @Test
    void testWithdrawAccountMustHaveSameCurrency() {
        Account source = accountService.create(USD);
        Account target = accountService.create(Currency.getInstance("EUR"));
        TransactionDto result = given().body("{\"currency\":\"USD\",\"amount\":100.0, \"source\":\"" + source.getId().toString() +"\", \"target\":\"" + target.getId().toString() +"\"}")
                .when().post("transactions/transfer")
                .then().statusCode(Statuses.CREATED)
                .extract().body().as(TransactionDto.class);

        assertThat(result.getStatus(), is("DECLINED"));
        assertThat(result.getReason(), is("Accounts must have same currency"));
    }

    @Test
    void testTransferNotEnoughMoney() {
        Account source = accountService.create(USD);
        Account target = accountService.create(USD);
        TransactionDto result = given().body("{\"currency\":\"USD\",\"amount\":100.0, \"source\":\"" + source.getId().toString() +"\", \"target\":\"" + target.getId().toString() +"\"}")
                .when().post("transactions/transfer")
                .then().statusCode(Statuses.CREATED)
                .extract().body().as(TransactionDto.class);

        assertThat(result.getStatus(), is("DECLINED"));
        assertThat(result.getReason(), is("Balance is less than requested amount"));
    }

    @Test
    void testTransfer() {
        Account source = accountService.create(USD);
        Account target = accountService.create(USD);
        transactionService.deposit(source.getId(), BigDecimal.valueOf(150.0), USD);
        TransactionDto result = given().body("{\"currency\":\"USD\",\"amount\":100.0, \"source\":\"" + source.getId().toString() +"\", \"target\":\"" + target.getId().toString() +"\"}")
                .when().post("transactions/transfer")
                .then().statusCode(Statuses.CREATED)
                .extract().body().as(TransactionDto.class);

        assertThat(result.getStatus(), is("COMPLETED"));
        assertThat(accountService.account(source.getId()).get().getBalance(), is(new BigDecimal("50.0000")));
        assertThat(accountService.account(target.getId()).get().getBalance(), is(new BigDecimal("100.0000")));
    }
}
