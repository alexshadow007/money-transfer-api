package com.github.alexshadow007.moneytransfer;

import com.github.alexshadow007.moneytransfer.db.DbModule;
import com.github.alexshadow007.moneytransfer.rest.AccountController;
import com.github.alexshadow007.moneytransfer.rest.ErrorController;
import com.github.alexshadow007.moneytransfer.rest.RestModule;
import com.github.alexshadow007.moneytransfer.rest.TransactionController;
import com.github.alexshadow007.moneytransfer.service.ServiceModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Runner {
    public static void main(String[] args) {
        Injector injector = Guice.createInjector(
                new ConfigModule(),
                new DbModule(),
                new ServiceModule(),
                new RestModule()
        );

        injector.getInstance(AccountController.class).init();
        injector.getInstance(TransactionController.class).init();
        injector.getInstance(ErrorController.class).init();
    }
}
