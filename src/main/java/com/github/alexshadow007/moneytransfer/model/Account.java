package com.github.alexshadow007.moneytransfer.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.UUID;

@Data
public class Account {
    private UUID id;
    private long version;
    private BigDecimal balance;
    private Currency currency;
}
