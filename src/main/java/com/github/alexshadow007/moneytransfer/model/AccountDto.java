package com.github.alexshadow007.moneytransfer.model;

import lombok.Data;

@Data
public class AccountDto {
    private String id;
    private double balance;
    private String currency;

    public AccountDto(Account account) {
        this.setId(account.getId().toString());
        this.setBalance(account.getBalance().doubleValue());
        this.setCurrency(account.getCurrency().getCurrencyCode());
    }
}
