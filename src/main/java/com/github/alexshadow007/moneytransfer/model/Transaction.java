package com.github.alexshadow007.moneytransfer.model;

import lombok.Data;

import java.util.UUID;

@Data
public class Transaction {
    private UUID id;
    private Status status;
    private String reason;

    public enum Status {
        COMPLETED, DECLINED, FAILED
    }
}
