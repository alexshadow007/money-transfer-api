package com.github.alexshadow007.moneytransfer.model;

import lombok.Data;

@Data
public class WithdrawDto {
    private String source;
    private Double amount;
    private String currency;
}
