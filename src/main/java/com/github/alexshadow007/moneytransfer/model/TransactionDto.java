package com.github.alexshadow007.moneytransfer.model;

import lombok.Data;

@Data
public class TransactionDto {
    private String id;
    private String status;
    private String reason;

    public TransactionDto(Transaction transaction) {
        this.setId(transaction.getId().toString());
        this.setStatus(transaction.getStatus().name());
        this.setReason(transaction.getReason());
    }
}
