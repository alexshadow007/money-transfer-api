package com.github.alexshadow007.moneytransfer.model;

import lombok.Data;

@Data
public class DepositDto {
    private String target;
    private Double amount;
    private String currency;
}
