package com.github.alexshadow007.moneytransfer.db;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.typesafe.config.Config;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.builder.xml.XMLMapperBuilder;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.session.SqlSessionManager;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;

import javax.sql.DataSource;

public class DbModule extends AbstractModule {

    @Provides
    @Singleton
    private HikariConfig createHikariConfig(Config config) {
        HikariConfig ret = new HikariConfig();

        ret.setJdbcUrl(config.getString("moneytransfer.db.url"));
        ret.setUsername(config.getString("moneytransfer.db.username"));
        ret.setPassword(config.getString("moneytransfer.db.password"));
        ret.setTransactionIsolation("TRANSACTION_READ_COMMITTED");
        ret.setMinimumIdle(1);

        return ret;
    }

    @Provides
    @Singleton
    private DataSource createDataSource(HikariConfig config) {
        return new HikariDataSource(config);
    }

    @Provides
    @Singleton
    private Environment createEnvironment(DataSource dataSource) {
        return new Environment("dev", new JdbcTransactionFactory(), dataSource);
    }

    @Provides
    @Singleton
    private SqlSessionFactory createSqlSessionFactory(Environment environment) {
        Configuration configuration = new Configuration(environment);

        configuration.getTypeHandlerRegistry().register(UUIDTypeHandler.class);
        configuration.getTypeHandlerRegistry().register(CurrencyTypeHandler.class);
        new XMLMapperBuilder(this.getClass().getResourceAsStream("/db/account-mapper.xml"), configuration,
                AccountMapper.class.getSimpleName(), configuration.getSqlFragments()).parse();

        return new SqlSessionFactoryBuilder().build(configuration);
    }

    @Provides
    @Singleton
    private SqlSessionManager createSqlSessionManager(SqlSessionFactory sqlSessionFactory) {
        return SqlSessionManager.newInstance(sqlSessionFactory);
    }
}
