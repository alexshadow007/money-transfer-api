package com.github.alexshadow007.moneytransfer.db;

import com.github.alexshadow007.moneytransfer.model.Account;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Currency;
import java.util.UUID;

public interface AccountMapper {
    void create(@Param("id") UUID id, @Param("currency") Currency currency);
    int update(@Param("id") UUID id, @Param("version") long version, @Param("balance") BigDecimal balance);
    void delete(@Param("id") UUID id);

    Collection<Account> accounts();
    Account account(@Param("id") UUID id);
}
