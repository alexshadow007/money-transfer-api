package com.github.alexshadow007.moneytransfer.rest;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ContentTypes {
    public static final String APPLICATION_JSON = "application/json";
}
