package com.github.alexshadow007.moneytransfer.rest;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Statuses {
    public static final int OK = 200;
    public static final int CREATED = 201;
    public static final int BAD_REQUEST = 400;
    public static final int NOT_FOUND = 404;
}
