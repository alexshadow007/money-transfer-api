package com.github.alexshadow007.moneytransfer.rest;

import com.google.gson.Gson;
import spark.Route;

public abstract class AbstractController implements Controller {
    protected final Gson gson;

    protected AbstractController(Gson gson) {
        this.gson = gson;
    }

    protected Route asJson(Route inner) {
        return (request, response) -> {
            Object result = inner.handle(request, response);
            response.type(ContentTypes.APPLICATION_JSON);
            return gson.toJson(result);
        };
    }
}
