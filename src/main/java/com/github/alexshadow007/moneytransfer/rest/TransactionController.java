package com.github.alexshadow007.moneytransfer.rest;

import com.github.alexshadow007.moneytransfer.model.*;
import com.github.alexshadow007.moneytransfer.service.TransactionService;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import spark.Service;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.UUID;

import static com.github.alexshadow007.moneytransfer.rest.ContentTypes.APPLICATION_JSON;
import static com.github.alexshadow007.moneytransfer.rest.Statuses.CREATED;

public class TransactionController extends AbstractController {

    private final Service spark;
    private final TransactionService transactionService;

    public TransactionController(Service spark, Gson gson, TransactionService transactionService) {
        super(gson);
        this.spark = spark;
        this.transactionService = transactionService;
    }

    @Override
    public void init() {
        spark.post("api/v1/transactions/deposit", APPLICATION_JSON, asJson((request, response) -> {
            DepositDto deposit = gson.fromJson(request.body(), DepositDto.class);
            verifyTarget(deposit.getTarget());
            verifyAmount(deposit.getAmount());
            verifyCurrency(deposit.getCurrency());

            Transaction result = transactionService.deposit(
                    UUID.fromString(deposit.getTarget()),
                    BigDecimal.valueOf(deposit.getAmount()),
                    Currency.getInstance(deposit.getCurrency())
            );

            response.status(CREATED);
            return new TransactionDto(result);
        }));

        spark.post("api/v1/transactions/withdraw", APPLICATION_JSON, asJson((request, response) -> {
            WithdrawDto withdraw = gson.fromJson(request.body(), WithdrawDto.class);
            verifySource(withdraw.getSource());
            verifyAmount(withdraw.getAmount());
            verifyCurrency(withdraw.getCurrency());

            Transaction result = transactionService.withdraw(
                    UUID.fromString(withdraw.getSource()),
                    BigDecimal.valueOf(withdraw.getAmount()),
                    Currency.getInstance(withdraw.getCurrency())
            );

            response.status(CREATED);
            return new TransactionDto(result);
        }));

        spark.post("api/v1/transactions/transfer", APPLICATION_JSON, asJson((request, response) -> {
            TransferDto transfer = gson.fromJson(request.body(), TransferDto.class);
            verifySource(transfer.getSource());
            verifyTarget(transfer.getTarget());
            verifyAmount(transfer.getAmount());
            verifyCurrency(transfer.getCurrency());

            Transaction result = transactionService.transfer(
                    UUID.fromString(transfer.getSource()),
                    UUID.fromString(transfer.getTarget()),
                    BigDecimal.valueOf(transfer.getAmount()),
                    Currency.getInstance(transfer.getCurrency())
            );

            response.status(CREATED);
            return new TransactionDto(result);
        }));
    }

    private void verifySource(String source) {
        if (Strings.isNullOrEmpty(source)) throw new IllegalArgumentException("Field [source] is required");
    }

    private void verifyTarget(String target) {
        if (Strings.isNullOrEmpty(target)) throw new IllegalArgumentException("Field [target] is required");
    }

    private void verifyAmount(Double amount) {
        if (amount == null) throw new IllegalArgumentException("Field [amount] is required");
        if (amount <= 0) throw new IllegalArgumentException("Field [amount] must have positive value");
    }

    private void verifyCurrency(String currency) {
        if (currency == null) throw new IllegalArgumentException("Field [currency] is required");
    }
}
