package com.github.alexshadow007.moneytransfer.rest;

import com.github.alexshadow007.moneytransfer.service.AccountService;
import com.github.alexshadow007.moneytransfer.service.TransactionService;
import com.google.gson.Gson;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.typesafe.config.Config;
import spark.Service;

public class RestModule extends AbstractModule {

    @Provides
    @Singleton
    private Service createSparkService(Config config) {
        Service ret = Service.ignite();
        ret.port(config.getInt("moneytransfer.rest.port"));
        return ret;
    }

    @Provides
    @Singleton
    private Gson createGson() {
        return new Gson();
    }

    @Provides
    @Singleton
    private AccountController createAccountController(Service spark, Gson gson, AccountService accountService) {
        return new AccountController(spark, gson, accountService);
    }

    @Provides
    @Singleton
    private TransactionController createTransactionController(Service spark, Gson gson, TransactionService transactionService) {
        return new TransactionController(spark, gson, transactionService);
    }

    @Provides
    @Singleton
    private ErrorController createErrorController(Service spark, Gson gson) {
        return new ErrorController(spark, gson);
    }
}
