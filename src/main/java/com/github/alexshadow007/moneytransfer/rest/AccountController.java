package com.github.alexshadow007.moneytransfer.rest;

import com.github.alexshadow007.moneytransfer.model.Account;
import com.github.alexshadow007.moneytransfer.model.AccountDto;
import com.github.alexshadow007.moneytransfer.model.ErrorDto;
import com.github.alexshadow007.moneytransfer.service.AccountService;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import spark.Service;

import java.util.Collection;
import java.util.Currency;
import java.util.Optional;
import java.util.UUID;

import static com.github.alexshadow007.moneytransfer.rest.Statuses.*;
import static java.util.stream.Collectors.toList;

public class AccountController extends AbstractController {

    private final Service spark;
    private final AccountService accountService;

    public AccountController(Service spark, Gson gson, AccountService accountService) {
        super(gson);
        this.spark = spark;
        this.accountService = accountService;
    }

    @Override
    public void init() {
        spark.post("api/v1/accounts", ContentTypes.APPLICATION_JSON, asJson((request, response) -> {
            AccountDto requestDto = gson.fromJson(request.body(), AccountDto.class);
            if (Strings.isNullOrEmpty(requestDto.getCurrency())) {
                throw new IllegalArgumentException("Field [currency] is required");
            }
            Account result = accountService.create(Currency.getInstance(requestDto.getCurrency()));
            response.status(CREATED);
            return new AccountDto(result);
        }));

        spark.get("api/v1/accounts", asJson((request, response) -> {
            Collection<Account> result = accountService.accounts();
            response.status(OK);
            return result.stream().map(AccountDto::new).collect(toList());
        }));

        spark.get("api/v1/accounts/:id", asJson((request, response) -> {
            UUID id = UUID.fromString(request.params("id"));
            Optional<Account> result = accountService.account(id);
            if (result.isPresent()) {
                response.status(OK);
                return new AccountDto(result.get());
            } else {
                response.status(NOT_FOUND);
                return new ErrorDto("Account with id [" + id.toString() + "] was not found");
            }
        }));

        spark.delete("api/v1/accounts/:id", asJson((request, response) -> {
            UUID id = UUID.fromString(request.params("id"));
            Optional<Account> account = accountService.account(id);
            if (account.isPresent()) {
                accountService.delete(account.get());
                response.status(OK);
                return new AccountDto(account.get());
            } else {
                response.status(NOT_FOUND);
                return new ErrorDto("Account with id [" + id.toString() + "] was not found");
            }
        }));
    }
}
