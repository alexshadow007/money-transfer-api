package com.github.alexshadow007.moneytransfer.rest;

import com.github.alexshadow007.moneytransfer.model.ErrorDto;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import spark.Service;

import static com.github.alexshadow007.moneytransfer.rest.ContentTypes.APPLICATION_JSON;
import static com.github.alexshadow007.moneytransfer.rest.Statuses.BAD_REQUEST;

public class ErrorController extends AbstractController {

    private final Service spark;

    public ErrorController(Service spark, Gson gson) {
        super(gson);
        this.spark = spark;
    }

    @Override
    public void init() {
        spark.exception(IllegalArgumentException.class, (exception, request, response) -> {
            response.status(BAD_REQUEST);
            response.type(APPLICATION_JSON);
            response.body(gson.toJson(new ErrorDto(exception.getMessage())));
        });
        spark.exception(JsonParseException.class, ((exception, request, response) -> {
            response.status(BAD_REQUEST);
            response.type(APPLICATION_JSON);
            response.body(gson.toJson(new ErrorDto(exception.getMessage())));
        }));

        spark.notFound(asJson((request, response) -> new ErrorDto("Requested resource was not found")));
        spark.internalServerError(asJson((request, response) -> new ErrorDto("Internal Server Error")));
    }
}
