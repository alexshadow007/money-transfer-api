package com.github.alexshadow007.moneytransfer;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class ConfigModule extends AbstractModule {

    @Provides
    @Singleton
    private Config createConfig() {
        return ConfigFactory.load();
    }

}
