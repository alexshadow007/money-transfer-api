package com.github.alexshadow007.moneytransfer.service;

import com.github.alexshadow007.moneytransfer.db.AccountMapper;
import com.github.alexshadow007.moneytransfer.model.Account;
import com.github.alexshadow007.moneytransfer.model.Transaction;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionManager;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.UUID;

import static com.github.alexshadow007.moneytransfer.model.Transaction.Status.*;
import static org.apache.ibatis.session.TransactionIsolationLevel.READ_COMMITTED;

public class TransactionServiceImpl implements TransactionService {

    private final SqlSessionManager sqlSessionManager;

    public TransactionServiceImpl(SqlSessionManager sqlSessionManager) {
        this.sqlSessionManager = sqlSessionManager;
    }

    @Override
    public Transaction deposit(UUID target, BigDecimal amount, Currency currency) {

        try (SqlSession session = sqlSessionManager.openSession(false)) {
            AccountMapper accountMapper = session.getMapper(AccountMapper.class);
            Account account = accountMapper.account(target);

            if (account == null) {
                return accountNotFound(target);
            }

            if (!account.getCurrency().equals(currency)) {
                return wrongCurrency(account.getCurrency());
            }

            if (accountMapper.update(account.getId(), account.getVersion(), account.getBalance().add(amount)) != 1) {
                return failed();
            }

            session.commit();
        }

        return completed();
    }

    @Override
    public Transaction withdraw(UUID source, BigDecimal amount, Currency currency) {

        try (SqlSession session = sqlSessionManager.openSession(false)) {
            AccountMapper accountMapper = session.getMapper(AccountMapper.class);
            Account account = accountMapper.account(source);

            if (account == null) {
                return accountNotFound(source);
            }

            if (!account.getCurrency().equals(currency)) {
                return wrongCurrency(account.getCurrency());
            }

            if (account.getBalance().compareTo(amount) < 0) {
                return notEnoughMoney();
            }

            if (accountMapper.update(account.getId(), account.getVersion(), account.getBalance().subtract(amount)) != 1) {
                return failed();
            }

            session.commit();
        }

        return completed();
    }

    @Override
    public Transaction transfer(UUID source, UUID target, BigDecimal amount, Currency currency) {

        try (SqlSession session = sqlSessionManager.openSession(false)) {
            AccountMapper accountMapper = session.getMapper(AccountMapper.class);

            Account sourceAccount = accountMapper.account(source);
            if (sourceAccount == null) {
                return accountNotFound(source);
            }

            Account targetAccount = accountMapper.account(target);
            if (targetAccount == null) {
                return accountNotFound(target);
            }

            if (!currency.equals(sourceAccount.getCurrency())) {
                return wrongCurrency(sourceAccount.getCurrency());
            }

            if (!sourceAccount.getCurrency().equals(targetAccount.getCurrency())) {
                return status(DECLINED, "Accounts must have same currency");
            }

            if (sourceAccount.getBalance().compareTo(amount) < 0) {
                return notEnoughMoney();
            }

            if (accountMapper.update(sourceAccount.getId(), sourceAccount.getVersion(), sourceAccount.getBalance().subtract(amount)) != 1) {
                return failed();
            }

            if (accountMapper.update(targetAccount.getId(), targetAccount.getVersion(), targetAccount.getBalance().add(amount)) != 1) {
                return failed();
            }

            session.commit();
        }

        return completed();
    }

    private Transaction status(Transaction.Status status) {
        return status(status, null);
    }

    private Transaction status(Transaction.Status status, String reason) {
        Transaction ret = new Transaction();
        ret.setId(UUID.randomUUID());
        ret.setStatus(status);
        ret.setReason(reason);

        return ret;
    }

    private Transaction accountNotFound(UUID id) {
        return status(DECLINED, "Account with id [" + id.toString() + "] was not found");
    }

    private Transaction wrongCurrency(Currency expected) {
        return status(DECLINED, "Currency must be [" + expected.getCurrencyCode() + "]");
    }

    private Transaction notEnoughMoney() {
        return status(DECLINED, "Balance is less than requested amount");
    }

    private Transaction failed() {
        return status(FAILED);
    }

    private Transaction completed() {
        return status(COMPLETED);
    }
}
