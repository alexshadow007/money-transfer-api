package com.github.alexshadow007.moneytransfer.service;

import com.github.alexshadow007.moneytransfer.model.Transaction;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.UUID;

public interface TransactionService {
    Transaction deposit(UUID target, BigDecimal amount, Currency currency);
    Transaction withdraw(UUID source, BigDecimal amount, Currency currency);
    Transaction transfer(UUID source, UUID target, BigDecimal amount, Currency currency);
}
