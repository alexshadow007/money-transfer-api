package com.github.alexshadow007.moneytransfer.service;

import com.github.alexshadow007.moneytransfer.model.Account;

import java.util.Collection;
import java.util.Currency;
import java.util.Optional;
import java.util.UUID;

public interface AccountService {
    Collection<Account> accounts();
    Optional<Account> account(UUID id);

    Account create(Currency currency);
    void delete(Account account);
}
