package com.github.alexshadow007.moneytransfer.service;

import com.github.alexshadow007.moneytransfer.db.AccountMapper;
import com.github.alexshadow007.moneytransfer.model.Account;
import org.apache.ibatis.session.SqlSessionManager;

import java.util.Collection;
import java.util.Currency;
import java.util.Optional;
import java.util.UUID;

public class AccountServiceImpl implements AccountService {
    private final AccountMapper accountMapper;

    public AccountServiceImpl(SqlSessionManager sqlSessionManager) {
        this.accountMapper = sqlSessionManager.getMapper(AccountMapper.class);
    }

    @Override
    public Collection<Account> accounts() {
        return accountMapper.accounts();
    }

    @Override
    public Optional<Account> account(UUID id) {
        return Optional.ofNullable(accountMapper.account(id));
    }

    @Override
    public Account create(Currency currency) {
        UUID id = UUID.randomUUID();
        accountMapper.create(id, currency);
        return accountMapper.account(id);
    }

    @Override
    public void delete(Account account) {
        accountMapper.delete(account.getId());
    }
}
