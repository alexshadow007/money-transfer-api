package com.github.alexshadow007.moneytransfer.service;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import org.apache.ibatis.session.SqlSessionManager;

public class ServiceModule extends AbstractModule {

    @Provides
    @Singleton
    private AccountService createAccountService(SqlSessionManager sqlSessionManager) {
        return new AccountServiceImpl(sqlSessionManager);
    }

    @Provides
    @Singleton
    private TransactionService createTransferService(SqlSessionManager sqlSessionManager) {
        return new TransactionServiceImpl(sqlSessionManager);
    }
}
