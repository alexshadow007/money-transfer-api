CREATE TABLE IF NOT EXISTS accounts(
    id VARCHAR(36) PRIMARY KEY,
    version BIGINT DEFAULT 1,
    deleted BOOLEAN DEFAULT false,
    balance DECIMAL(19, 4) DEFAULT 0.0,
    currency VARCHAR(3) NOT NULL
);

CREATE INDEX IF NOT EXISTS accounts_deleted ON accounts(deleted);