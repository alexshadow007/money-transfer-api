# Money Transfer API
A RESTful API for money transfers between accounts.

## How to build the application
```
    ./gradlew clean build
```

## How to run tests
```
    ./gradlew clean check
```

## How to run the application
You can run
```
./gradlew clean run
```
Or build application and then run
```
java -jar build/libs/money-transfer-api-1.0-SNAPSHOT.jar
```
The application runs on port 8080

## How to use the application
### Accounts

#### Create Account
This endpoint creates account.
##### Request
```
POST http://localhost:8080/api/v1/accounts
{
    "currency":"USD"
}
```
Field|Description|Format
-----|-----------|-----
currency|The account currency|3-letter ISO currency code

##### Response
```
{  
      "id":"f342e7f6-b0d0-4eee-b6e9-018bd86960a4",
      "balance":0.0,
      "currency":"USD"
}
```
Field|Description|Format
-----|-----------|-----
id|The account ID|UUID
balance|The available balance|Decimal
currency|The account currency|3-letter ISO currency code

#### Get Accounts
This endpoint retrieves accounts.
##### Request
```
GET http://localhost:8080/api/v1/accounts
```

##### Response
```
[  
   {  
      "id":"f342e7f6-b0d0-4eee-b6e9-018bd86960a4",
      "balance":0.0,
      "currency":"USD"
   },
   {  
      "id":"68c73d49-151b-4559-b104-de2316776642",
      "balance":0.0,
      "currency":"USD"
   }
]
```
Field|Description|Format
-----|-----------|-----
id|The account ID|UUID
balance|The available balance|Decimal
currency|The account currency|3-letter ISO currency code

#### Get Account
This endpoint retrieves account by ID.
##### Request
```
GET http://localhost:8080/api/v1/accountd/<id>
```
Parameter|Description|Format
---------|-----------|------
id|The account ID|UUID

##### Response
```
{  
      "id":"f342e7f6-b0d0-4eee-b6e9-018bd86960a4",
      "balance":0.0,
      "currency":"USD"
}
```
Field|Description|Format
-----|-----------|-----
id|The account ID|UUID
balance|The available balance|Decimal
currency|The account currency|3-letter ISO currency code

#### Delete Account
This endpoint deletes account by ID.
##### Request
```
DELETE http://localhost:8080/api/v1/accounts/<id>
```
Parameter|Description|Format
---------|-----------|------
id|The account ID|UUID

### Transactions
#### Deposit
This endpoints processes deposit transaction for account.
##### Request
```
POST localhost:8080/api/v1/transactions/deposit
{
    "target":"f342e7f6-b0d0-4eee-b6e9-018bd86960a4",
    "amount":100.00,
    "currency":"USD"
}
```
Field|Description|Format
-----|-----------|-----
target|The ID of target account|UUID
amount|The transaction amount|Decimal
currency|The transaction currency, target account should be in this currency|3-letter ISO currency code

#### Response
```
{
    "id":"a125719b-42ed-47fe-b5de-1998be7e1040",
    "status":"COMPLETED"
}
```
Field|Description|Format
-----|-----------|-----
id|The transaction ID|UUID
status|The transaction status. Can be `COMPLETED`, `DECLINED` or `FAILED`|Text
reason|The reason for `DECLINED` or `FAILED` transaction status|Text

#### Withdraw
This endpoints processes withdrawal transaction for account.
##### Request
```
POST localhost:8080/api/v1/transactions/withdraw
{
    "source":"f342e7f6-b0d0-4eee-b6e9-018bd86960a4",
    "amount":100.00,
    "currency":"USD"
}
```
Field|Description|Format
-----|-----------|-----
target|The ID of target account|UUID
amount|The transaction amount|Decimal
currency|The transaction currency, source account should be in this currency|3-letter ISO currency code

#### Response
```
{
    "id":"a125719b-42ed-47fe-b5de-1998be7e1040",
    "status":"COMPLETED"
}
```
Field|Description|Format
-----|-----------|-----
id|The transaction ID|UUID
status|The transaction status. Can be `COMPLETED`, `DECLINED` or `FAILED`|Text
reason|The reason for `DECLINED` or `FAILED` transaction status|Text

#### Transfer
This endpoints processes transfer transaction between accounts.
##### Request
```
POST localhost:8080/api/v1/transactions/transfer
{
    "source":"f342e7f6-b0d0-4eee-b6e9-018bd86960a4",
    "target":"68c73d49-151b-4559-b104-de2316776642",
    "amount":100.00,
    "currency":"USD"
}
```
Field|Description|Format
-----|-----------|-----
source|The ID of source account|UUID
target|The ID of target account|UUID
amount|The transaction amount|Decimal
currency|The transaction currency, both source and target accounts should be in this currency|3-letter ISO currency code

#### Response
```
{
    "id":"a125719b-42ed-47fe-b5de-1998be7e1040",
    "status":"COMPLETED"
}
```
Field|Description|Format
-----|-----------|-----
id|The transaction ID|UUID
status|The transaction status. Can be `COMPLETED`, `DECLINED` or `FAILED`|Text
reason|The reason for `DECLINED` or `FAILED` transaction status|Text